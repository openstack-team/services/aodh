aodh (19.0.0-2) unstable; urgency=medium

  * Removed -O--buildsystem=python_distutils from d/rules.

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Dec 2024 11:36:35 +0100

aodh (19.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Oct 2024 13:14:15 +0200

aodh (19.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Sep 2024 16:25:13 +0200

aodh (19.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed Fix_db_upgrade_with_SQLAlchemy_2.0.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2024 08:20:08 +0200

aodh (18.0.0-2) unstable; urgency=medium

  * Add Fix_db_upgrade_with_SQLAlchemy_2.0.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 08 May 2024 15:54:03 +0200

aodh (18.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 04 Apr 2024 08:58:38 +0200

aodh (18.0.0~rc1-1) experimental; urgency=medium

  * Add close-on-exec{2,} = true in uwsgi config.
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed lsb-base depends.

 -- Thomas Goirand <zigo@debian.org>  Sat, 16 Mar 2024 18:43:03 +0100

aodh (17.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 23:14:28 +0200

aodh (17.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Blacklist TestEvaluatorBaseClass.test_base_time_constraints_timezone().
  * Cleans better.

 -- Thomas Goirand <zigo@debian.org>  Sat, 16 Sep 2023 11:23:12 +0200

aodh (16.0.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 13:39:25 +0200

aodh (16.0.0-2) experimental; urgency=medium

  * Build-depends on openstack-pkg-tools (>= 123~).

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 Apr 2023 10:31:30 +0200

aodh (16.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Mar 2023 13:07:35 +0100

aodh (16.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bookworm.

 -- Thomas Goirand <zigo@debian.org>  Fri, 03 Mar 2023 16:17:29 +0100

aodh (15.0.0-2) unstable; urgency=medium

  * Add add-a-healthcheck_disable-file.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Nov 2022 16:49:01 +0100

aodh (15.0.0-1) unstable; urgency=medium

  * Start after FRR.
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Oct 2022 22:33:41 +0200

aodh (15.0.0~rc1-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 14:54:25 +0200

aodh (15.0.0~rc1-2) experimental; urgency=medium

  * Add missing oslo.reports namespace when generating config file.

 -- Thomas Goirand <zigo@debian.org>  Wed, 21 Sep 2022 12:25:33 +0200

aodh (15.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 13 Sep 2022 14:33:37 +0200

aodh (14.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 16:06:34 +0200

aodh (14.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 14:56:30 +0100

aodh (14.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Mar 2022 21:00:17 +0100

aodh (13.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 16:58:05 +0200

aodh (13.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fix 00_default_policy.yaml.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 20:55:13 +0200

aodh (13.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed --with systemd from debian/rules.

 -- Thomas Goirand <zigo@debian.org>  Wed, 15 Sep 2021 13:58:27 +0200

aodh (12.0.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 11:58:30 +0200

aodh (12.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Apr 2021 22:56:40 +0200

aodh (12.0.0~rc1-1) experimental; urgency=medium

  * Tune aodh-api-uwsgi.ini for performance.
  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.
  * Fixed (build-)depends for this release.
  * Add remove-httpdomain-sphinx-ext.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Mar 2021 13:04:20 +0100

aodh (11.0.0-2) unstable; urgency=medium

  * mv /etc/aodh/policy.json /etc/aodh/disabled.policy.json.old instead of
    deleting /etc/aodh/policy.json.

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Dec 2020 11:12:55 +0100

aodh (11.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed watch file.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2020 17:03:36 +0200

aodh (11.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed python3-jsconschema version for this release.
  * Use /etc/aodh/policy.d/00_default_policy.yaml.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Sep 2020 14:33:35 +0200

aodh (10.0.0-2) unstable; urgency=medium

  * Add remove-wsmeext.sphinxext.patch (Closes: #963634).

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Sep 2020 10:44:25 +0200

aodh (10.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 May 2020 15:34:25 +0200

aodh (10.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 09 May 2020 23:16:27 +0200

aodh (10.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Apr 2020 23:13:15 +0200

aodh (9.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 22:32:05 +0200

aodh (9.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Oct 2019 15:05:02 +0200

aodh (9.0.0~rc1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 26 Sep 2019 16:27:15 +0200

aodh (8.0.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 14:48:22 +0200

aodh (8.0.0-2) experimental; urgency=medium

  * d/control:
      - Bump openstack-pkg-tools to version 99
      - Add me to uploaders field
  * d/copyright: Add me to copyright file

 -- Michal Arbet <michal.arbet@ultimum.io>  Fri, 03 May 2019 18:12:19 +0200

aodh (8.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Apr 2019 08:48:01 +0200

aodh (8.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed all package versions satisfied in Buster.
  * Removed python3- prefix when calling config generators.

 -- Thomas Goirand <zigo@debian.org>  Fri, 29 Mar 2019 15:20:37 +0100

aodh (7.0.0-5) unstable; urgency=medium

  * Fix Aodh default port to 8042 for uwsgi. The 9696 was wrong, that's the
    port for neutron-api.

 -- Thomas Goirand <zigo@debian.org>  Fri, 01 Mar 2019 15:08:35 +0100

aodh (7.0.0-4) unstable; urgency=medium

  * Removed mongodb build-depends (Closes: #919087).

 -- Thomas Goirand <zigo@debian.org>  Thu, 17 Jan 2019 16:25:48 +0100

aodh (7.0.0-3) unstable; urgency=medium

  * Also remove /etc/aodh on purge. (Closes: #909608).

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Jan 2019 09:04:47 +0100

aodh (7.0.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Echo something when starting mongod, as it is doing readline.
  * Remove /etc/aodh/{aodh.conf,api-paste.ini,policy.json} on purge.
    (Closes: #905496)

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Aug 2018 11:37:20 +0200

aodh (7.0.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 23 Aug 2018 22:39:12 +0200

aodh (6.0.0-8) unstable; urgency=medium

  * Correctly configures aodh-api through uwsgi.ini.

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 Jun 2018 11:00:58 +0200

aodh (6.0.0-7) unstable; urgency=medium

  * Also install app.wsgi in /usr/share/aodh, needed for puppet-openstack.

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 May 2018 12:16:18 +0200

aodh (6.0.0-6) unstable; urgency=medium

  * Add --rem-header Content-Lenght when starting aodh-api under uwsgi.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 May 2018 11:38:25 +0200

aodh (6.0.0-5) unstable; urgency=medium

  * Fixed postrm.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Mar 2018 16:11:52 +0000

aodh (6.0.0-4) unstable; urgency=medium

  * Explicitly using python3- version of generators when building confs.
  * Call pkgos-readd-keystone-authtoken-missing-options.

 -- Thomas Goirand <zigo@debian.org>  Wed, 07 Mar 2018 11:18:04 +0000

aodh (6.0.0-3) unstable; urgency=medium

  * Uploading to unstable.
  * Removed python3-gnocchi from build-depends.

 -- Thomas Goirand <zigo@debian.org>  Tue, 27 Feb 2018 08:48:28 +0000

aodh (6.0.0-2) experimental; urgency=medium

  * Fixed py3 sheebang.
  * Using pkgos-dh_auto_test to run unit tests.

 -- Thomas Goirand <zigo@debian.org>  Thu, 22 Feb 2018 10:34:06 +0000

aodh (6.0.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Generate policy.yaml using oslopolicy-policy-generator.
  * Standards-Version is now 4.1.3.
  * Fix aodh-doc short desc.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Feb 2018 21:09:54 +0000

aodh (5.0.0-8) unstable; urgency=medium

  * Team upload.
  * Updated French translation of the debconf templates (Closes: #885587).

 -- Ondřej Nový <onovy@debian.org>  Sun, 21 Jan 2018 15:35:56 +0100

aodh (5.0.0-7) unstable; urgency=medium

  * Fixed debian/copyright attributions and years.

 -- Thomas Goirand <zigo@debian.org>  Fri, 29 Dec 2017 10:43:19 +0100

aodh (5.0.0-6) unstable; urgency=medium

  * Switch to Python 3 (Closes: #884636).
  * Fixed aodh-api short description.
  * Correctly purge aodh-common's db on purge.
  * Install missing alembic scripts.
  * Using python3 -m sphinx to build sphinx doc.

 -- Thomas Goirand <zigo@debian.org>  Fri, 22 Dec 2017 09:38:19 +0100

aodh (5.0.0-5) unstable; urgency=medium

  * Team upload.
  * Fix wrong key in debconf (Closes: #884265)
  * Don't depend od dh-systemd, it's not needed
  * Remove duplicity in B-D
  * Add aodh-api depends on lsb-base
  * Enable autopkgtest-pkg-python testsuite

 -- Ondřej Nový <onovy@debian.org>  Wed, 13 Dec 2017 11:36:27 +0100

aodh (5.0.0-4) unstable; urgency=medium

  * Do not customize connection= in debian/rules.
  * Added --paste-logger to uwsgi_python params to log python output.
  * Added python-pastescript as runtime depends for aodh-api, needed for the
    --paste-logger option.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Dec 2017 09:39:36 +0000

aodh (5.0.0-3) unstable; urgency=medium

  * Handle the db with dbconfig-common, now that Aodh switched away from
    mongodb.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Dec 2017 09:20:39 +0000

aodh (5.0.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Updated pt_BR.po (Closes: #861981).

 -- Thomas Goirand <zigo@debian.org>  Sun, 29 Oct 2017 20:24:07 +0100

aodh (5.0.0-1) experimental; urgency=medium

  * Updated Danish translation of the debconf templates (Closes: #830646).
  * Do not run test_messaging.MessagingTests.test_get_transport_optional,
    which is failing under some OS (Xenial & Jessie arm64).
  * New upstream release.
  * Changed maintainer name.
  * Ran wrap-and-sort -bast.
  * Updated VCS fields.
  * Updating standards version to 4.1.1.
  * Fixed (build-)depends for this release.
  * Removed CVE patch, applied upstream.
  * Installs correctly api-paste.ini app.wsgi policy.json from new location.

 -- Thomas Goirand <zigo@debian.org>  Sun, 15 Oct 2017 23:25:42 +0200

aodh (3.0.0-4+deb9u1) stretch-security; urgency=medium

  * CVE-2017-12440: apply upstream patch (Closes: #872605).

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Aug 2017 00:59:49 +0200

aodh (3.0.0-4) unstable; urgency=medium

  * Team upload.
  * Revert net-tools dependency.
  * Bump build dependency on openstack-pkg-tools (Closes: #858215).

 -- David Rabel <david.rabel@noresoft.com>  Sat, 01 Apr 2017 11:11:56 +0200

aodh (3.0.0-3) unstable; urgency=medium

  [ David Rabel ]
  * Team upload.
  * Dependency added: net-tools (Closes: #858215)

 -- David Rabel <david.rabel@noresoft.com>  Sun, 19 Mar 2017 22:11:59 +0100

aodh (3.0.0-2) unstable; urgency=medium

  * Correctly kills mongodb processes (Closes: #835177).

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 Oct 2016 13:55:42 +0200

aodh (3.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2016 23:18:52 +0200

aodh (3.0.0~rc1-3) unstable; urgency=medium

  [ Thomas Goirand ]
  * Uploading to unstable.
  * Removed --namespace oslo.service.service when generating aodh.conf.
  * debian/source/options: ignores egg-info and .gitreview.
  * Fixed EPOCH for oslotest.
  * Build-Depends on openstack-pkg-tools >= 53~.
  * Switch aodh-api to uwsgi-plugin-python, since eventlet support is gone
    upstream.
  * Debconf translation:
    - it (Closes: #839199).

  [ Ondřej Nový ]
  * d/control: Use correct branch in Vcs-* fields

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Sep 2016 09:09:28 +0200

aodh (3.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Add python-zaqarclient as build-depends-indep.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Sep 2016 17:14:50 +0200

aodh (3.0.0~b3-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using OpenStack's Gerrit as VCS URLs.
  * Modified .gitreview to point at Gerrit.
  * Add debian/functions.sh as it's gone upstream.
  * Add -Xusr/etc/aodh/aodh-config-generator.conf in dh_install call.
  * Updated Danish translation of the debconf templates (Closes: #830646).
  * Do not run test_messaging.MessagingTests.test_get_transport_optional,
    which is failing under some OS (Xenial & Jessie arm64).

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

 -- Thomas Goirand <zigo@debian.org>  Mon, 11 Jul 2016 14:30:47 +0200

aodh (2.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)

  [ Thomas Goirand ]
  * Updated Portuguese translation for debconf messages (Closes: #821240).
  * Updated Japanese debconf templates translation (Closes: #820759).
  * Updated Dutch debconf templates translation (Closes: #823253).

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 May 2016 11:25:47 +0200

aodh (2.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 07 Apr 2016 18:37:08 +0200

aodh (2.0.0~rc1-3) unstable; urgency=medium

  * Uplodaing to unstable.
  * Updated ja.po debconf translation (Closes: #819132).

 -- Thomas Goirand <zigo@debian.org>  Tue, 05 Apr 2016 10:56:39 +0200

aodh (2.0.0~rc1-2) experimental; urgency=medium

  * Not using keystone admin token anymnore to registre endpoints.
  * Ran debconf-updatepo.

 -- Thomas Goirand <zigo@debian.org>  Tue, 29 Mar 2016 12:06:25 +0000

aodh (2.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Set OS_TEST_PATH=$(CURDIR)/aodh/tests/unit to run unit tests.

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Mar 2016 09:20:30 +0100

aodh (2.0.0~b3-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Fixed path of tests when running testr run.
  * Using script in debian to start mongo.
  * Fixed oslo-config-generator namespace list.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Mar 2016 23:37:27 +0800

aodh (2.0.0~b2-2) experimental; urgency=medium

  * Updated nl.po debconf translation (Closes: #806491).
  * VCS URLs using HTTPS.

 -- Thomas Goirand <zigo@debian.org>  Sat, 06 Feb 2016 05:41:51 +0000

aodh (2.0.0~b2-1) experimental; urgency=medium

  [ Corey Bryant ]
  * d/control: Add python-pymongo to python-aodh depends as aodh-api init
    script fails without it.
  * d/rules: Ensure that alembic files are installed correctly.

  [ Thomas Goirand ]
  * New upstream release.
  * Fix where the alembic files are stored.
  * Fixed debian/copyright ordering.
  * Fixed namespace for generating config file.

 -- Thomas Goirand <zigo@debian.org>  Thu, 14 Jan 2016 06:58:06 +0800

aodh (2.0.0~b1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Dec 2015 11:09:38 +0100

aodh (1.0.0-11) unstable; urgency=medium

  * Fixed incorrect python-keystonemiddleware (build-)depends version.

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Nov 2015 14:20:29 +0100

aodh (1.0.0-10) unstable; urgency=medium

  * Import .po files from Glance (Closes: #802183).
  * Added pt.po debconf translation (Closes: #802423).

 -- Thomas Goirand <zigo@debian.org>  Fri, 13 Nov 2015 09:48:15 +0100

aodh (1.0.0-9) unstable; urgency=medium

  * Added q-text-as-data as depends for aodh-api.

 -- Thomas Goirand <zigo@debian.org>  Tue, 03 Nov 2015 11:23:14 +0000

aodh (1.0.0-8) unstable; urgency=medium

  * Rebuilt with openstack-pkg-tools 37 to use Keystone API v3.

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Nov 2015 20:08:59 +0000

aodh (1.0.0-7) unstable; urgency=medium

  * Correctly purge remaining folders on purge (Closes: #802534).

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Oct 2015 12:15:10 +0000

aodh (1.0.0-6) unstable; urgency=medium

  * Added missing Alembic migration scripts (LP: 1508463).

 -- Thomas Goirand <zigo@debian.org>  Thu, 22 Oct 2015 08:01:49 +0000

aodh (1.0.0-5) unstable; urgency=medium

  * Added python-pymysql as runtime dependency.

 -- Thomas Goirand <zigo@debian.org>  Tue, 13 Oct 2015 14:57:51 +0200

aodh (1.0.0-4) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2015 12:53:18 +0000

aodh (1.0.0-3) experimental; urgency=medium

  * Added a -expirer daemon, after request from upstream.
  * Minimum version for ceilometerclient is now 1.5.0.

 -- Thomas Goirand <zigo@debian.org>  Tue, 06 Oct 2015 15:14:51 +0000

aodh (1.0.0-2) experimental; urgency=medium

  * Now aodh-api depends on openstackclient.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Sep 2015 12:51:21 +0000

aodh (1.0.0-1) experimental; urgency=medium

  * Initial release. (Closes: #798570)

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 Sep 2015 17:50:46 +0200
